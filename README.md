# Mov
 It is a simple Application to import `Person` from `.csv` file.
 `.csv` file should look like:
 
 ```
 Name,Email Address,Telephone Number,Website
 Coy Kunde,,0800 382630,mills.net
 Domenick Rau,domenick_rau@bashirianarmstrong.biz,01139 23402,
 ,fahey_shane@prohaskadickens.co,01537 23495,koepp.info
 Cordia Borer,cordia.borer@murazik.io,01686 08200,https://harvey.biz
 ```
 
## Setup

### Install dependencies
```bash
bundle install
```

### Database setup
* `config/database.yml` files is already inside this application
```bash
rake db:create
rake db:schema:load
```

### Run server
```bash
rails s
```

## Test
This project contains `RSpec` tests. To run tests:

```bash
rspec spec
```

# Assignment Details

Write an app that will take this CSV file and import it into a database.

Rules:

    Rows without a name or email address should not be imported
    Email addresses must be unique across records

What we’re looking for, apart from your general approach:

    Testing
    Error handling
    A basic level of documentation
    Small, descriptive commits

