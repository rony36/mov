require 'csv'
class ImportCSV
  attr_reader :csv_file
  PERSON_FIELDS = %i(name email telephone_number website).freeze
  IMPORT_OPTIONS = { recursive: false, validate: false }.freeze

  def self.call(csv_file)
    new(csv_file).call
  end

  def initialize(csv_file)
    @csv_file = csv_file
  end

  def call
    # For bulk huge data I rather prefer to use `activerecord-import`
    # Person.import(PERSON_FIELDS.dup, people, IMPORT_OPTIONS)
    ::CSV.foreach(csv_file.path).map do |person|
      person = Person.new Hash[PERSON_FIELDS.zip(person)]
      next unless person.save
      person.reload
    end.compact
  rescue => e
    $stdout.puts "Error: #{e.message}"
    false
  end
end
