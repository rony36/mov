class PeopleController < ApplicationController
  def index
    @people = Person.all
  end

  def import
    if people = ImportCSV.call(person_params[:import_file])
      redirect_to people_path, notice: "#{people.try(:count) || 0} People Imported Sucessfully"
    else
      redirect_to root_path, notice: 'Something Went Wrong!'
    end
  end

  private

  def person_params
    params.require(:person).permit(:import_file)
  end
end
