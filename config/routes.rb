Rails.application.routes.draw do
  resource :dashboard, only: %i(show)
  resources :people, only: %i(index) do
    collection do
      post :import
    end
  end
  root 'dashboard#show'
end
