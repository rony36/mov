FactoryGirl.define do
  factory :person do
    name 'John Doe'
    sequence(:email) { |i| "user_#{i}@example.com" }
    telephone_number '12121'
    website 'abc.com'
  end
end
