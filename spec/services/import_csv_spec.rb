require 'spec_helper'

RSpec.describe ImportCSV do
  let(:import_file) do
    OpenStruct.new(path: 'some-path')
  end

  context '.call' do
    let(:import_csv_double) { instance_double described_class }
    before do
      allow(described_class).to receive(:new).with(import_file).and_return(true)
    end

    it 'should call call' do
      expect(described_class).to receive(:new).with(import_file).and_return(import_csv_double)
      expect(import_csv_double).to receive(:call).and_return(true)
      described_class.call(import_file)
    end
  end

  context '#call' do
    let(:instance) { described_class.new(import_file) }
    let(:importable_data) do
      [
        ['Coy Kunde', nil, '0800 382630', 'mills.net'],
        [nil , 'fahey_shane@prohaskadickens.co', '01537 23495', 'koepp.info'],
        ['Tristån Gaylord', 'triston_gaylord@runolfon.org', '0121 031 8172', 'turcotte.org']
      ]
    end
    context 'successful importation' do
      before do
        allow(CSV).to receive(:foreach).and_return(importable_data)
      end

      it 'should return an array' do
        expect(instance.call).to be_an(Array)
      end

      it 'should return an array of person' do
        expect(instance.call.first).to be_a(Person)
      end

      it 'should import only valid person' do
        expect(instance.call.count).to eq 1
      end
    end

    context 'unsuccess importation' do
      before do
        allow(CSV).to receive(:foreach).and_raise(StandardError)
      end

      it 'should return false' do
        expect(instance.call).to eq false
      end
    end
  end
end
