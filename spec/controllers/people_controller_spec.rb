RSpec.describe PeopleController, type: :controller do
  describe 'GET #index' do
    subject { get :index }
    let!(:person) { FactoryGirl.create :person }

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'assigns @people variable' do
      subject
      expect(assigns(:people)).to match_array([person])
    end
  end

  describe 'POST #import' do
    subject { post :import, params: { person: { import_file: 'some_file' } } }

    context 'People imported successfully' do
      before do
        allow(ImportCSV).to receive(:call).and_return(true)
      end

      it 'should redirect to person index' do
        subject
        expect(response).to redirect_to people_path
      end
    end

    context 'People imported unsuccessfully' do
      before { allow(ImportCSV).to receive(:call).and_return(false) }

      it 'should redirect to dashboard' do
        subject
        expect(response).to redirect_to root_path
      end
    end
  end
end
