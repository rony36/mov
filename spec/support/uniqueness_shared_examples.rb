RSpec.shared_examples_for 'database field uniqueness' do |model, field, valid_value|
  describe "#{field} uniqueness" do
    let(:params) { { field => (valid_value || 'existing.email@example.com') } }
    subject { FactoryGirl.build model, params }

    before { FactoryGirl.create model, params }
    it { is_expected.to validate_uniqueness_of(field) }
  end
end
