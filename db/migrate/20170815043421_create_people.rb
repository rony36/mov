class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |table|
      table.string :name, null: false
      table.string :email, null: false, unique: true
      table.string :telephone_number
      table.string :website
      table.timestamps
    end
  end
end
